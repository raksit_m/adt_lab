
#include <iostream>

#include <math.h>

using namespace std;

bool check_licence(string licence)
{
	string alphabet = licence.substr(0, 3);

	string number = licence.substr(4, 4);


	int alphabet_value = (int) ((alphabet.at(0)-65) * pow(26, 2) + (alphabet.at(1)-65) * pow(26, 1) + (alphabet.at(2)-65) * pow(26, 0));

	int number_value = 0;

	for(int i = 0; i < number.length(); i++) {

		int temp = number.at(i) - '0';

		number_value += (temp)*(int)(pow(10, number.length() - 1 - i));
	}

	if(abs(alphabet_value - number_value) <= 100) {

		return true;
	}

	return false;
}

int main()
{
	int length;

	cin >> length;

	string array [length];

	for(int i = 0; i < length; i++) {

		cin >> array[i];
	}

	for(int i = 0; i < length; i++) {

		bool check = check_licence(array[i]);

		if(check) cout << "nice" << endl;

		else cout << "not nice" << endl;

	}
}
