
#include <iostream>

#include <map>

#include <vector>

using namespace std;

int main()
{
	map<int, int> map;

	vector<int> vector;

	int n;

	while(scanf("%d", &n) == 1) {

		if(map[n] == 0) {

			vector.push_back(n);

		}

		map[n]++;
	}

	for(int i = 0; i < (int)vector.size(); i++) {

		cout << vector[i] << " " << map[vector[i]] << endl;

	}

	return 0;
}
