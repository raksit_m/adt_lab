
#include <iostream>

using namespace std;

string find_telephone(string input)
{
	string output;

	for(int i = 0; i < (signed)input.length(); i++) {

		if(input.at(i) == 'A' || input.at(i) == 'B' || input.at(i) == 'C') {
			output += '2';
		}

		else if(input.at(i) == 'D' || input.at(i) == 'E' || input.at(i) == 'F') {
			output += '3';
		}

		else if(input.at(i) == 'G' || input.at(i) == 'H' || input.at(i) == 'I') {
			output += '4';
		}

		else if(input.at(i) == 'J' || input.at(i) == 'K' || input.at(i) == 'L') {
			output += '5';
		}

		else if(input.at(i) == 'M' || input.at(i) == 'N' || input.at(i) == 'O') {
			output += '6';
		}

		else if(input.at(i) == 'P' || input.at(i) == 'Q' || input.at(i) == 'R' || input.at(i) == 'S') {
			output += '7';
		}

		else if(input.at(i) == 'T' || input.at(i) == 'U' || input.at(i) == 'V') {
			output += '8';
		}

		else if(input.at(i) == 'W' || input.at(i) == 'X' || input.at(i) == 'Y' || input.at(i) == 'Z') {
			output += '9';
		}

		else {
			output += input.at(i);
		}
	}

	return output;
}

int main()
{
	while(!cin.eof()) {

		string input;

		getline(cin, input);

		cout << find_telephone(input) << endl;
	}
}
