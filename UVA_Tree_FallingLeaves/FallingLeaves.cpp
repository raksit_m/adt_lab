#include <iostream>

using namespace std;

typedef int valueType;

struct TreeNode
{
	valueType val;
	TreeNode* left;
	TreeNode* right;

	TreeNode(valueType val, TreeNode *left=0, TreeNode *right=0)
	:val(val), left(left), right(right) {}
};

void check(TreeNode* r)
{
	/* the code for checking is hidden. */
}

void inorder(TreeNode* r)
{
	if(!r) {
		return;
	}

	inorder(r -> left);

	cout << r -> val << endl;

	inorder(r -> right);

}

void insert(TreeNode*& r, valueType x)
{
	if(!r) {
		r = new TreeNode(x);
	}

	else if(x < r-> val) {
		insert(r -> left, x);
	}

	else if(x > r-> val) {
		insert(r -> right, x);
	}
}

TreeNode* find(TreeNode* r, valueType x)
{
	if(!r) return r;

	if(r -> val == x) return r;

	if(r -> val < x) return find(r -> right, x);

	else return find(r -> left, x);
}

int main()
{

}

