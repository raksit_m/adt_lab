
#include <iostream>

#include <list>

using namespace std;

int main()
{
	string s;

	list<char> chars;

	while(cin >> s) {

		chars.clear();

		list<char>:: iterator it = chars.begin();

		for(char c : s) {

			if(c == '[') it = chars.begin();

			else if(c == ']') it = chars.end();

			else chars.insert(it, c);
		}

		for(list<char>::iterator it2 = chars.begin(); it2 != chars.end(); it2++) {

			cout << *it2;

		}

		cout << '\n';
	}
}
