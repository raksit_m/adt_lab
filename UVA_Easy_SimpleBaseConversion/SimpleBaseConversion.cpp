
#include <iostream>

#include <cstdio>

#include <cstdlib>

using namespace std;

int main()
{
	while (true) {

		char number[16];

		scanf("%s", number);

		if (number[0] == '0' && number[1] == 'x')

			cout << strtol(number, NULL, 16) << endl;

		else {

			int n = strtol(number, NULL, 10);

			if (n < 0)
				break;

			else

				printf("0x%X\n", n);

		}

	}
	return 0;
}
