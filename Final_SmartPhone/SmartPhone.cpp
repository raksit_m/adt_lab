
#include <iostream>

using namespace std;

int main()
{
	long n, k;

	cin >> n >> k;

	long array[n][4];

	for(int i = 0; i < n; i++) {

		cin >> array[i][0];

		cin >> array[i][1];

		cin >> array[i][2];

		cin >> array[i][3];
	}

	for(int i = 0; i < k; i++) {

		long type, temp;

		cin >> type >> temp;

		long minPrice = 0;

		int index = -1;

		long minTemp = 0;

		for(int j = 0; j < n; j++) {

			if(array[j][type-1] >= temp && (array[j][type-1] < minTemp || minTemp == 0)) {

				minTemp = array[j][type-1];

				minPrice = array[j][3];

				index = j;
			}
		}

		cout << minPrice << endl;

		if(index != -1) {

			for(int k = 0; k < 3; k++) {

				array[index][k] = 0;
			}
		}

		minTemp = 0;

		minPrice = 0;

		index = -1;
	}
}
