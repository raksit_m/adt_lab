
#include <iostream>

using namespace std;

class Queue
{
private:

	struct Node {

		pair<int, int> datum;

		Node* next;

		Node(int id, int age, Node* nxt=0) {

			datum = make_pair(id, age);

			next = nxt;

		}

	};

	Node* first_node;

public:

	Queue();

	~Queue();

	void enqueue(int id, int age);

	pair<int, int> dequeue();

	int size();
};

Queue::Queue()
{
	first_node = 0;
}

Queue::~Queue()
{
	while(first_node != 0) dequeue();
}

int Queue::size()
{
	if(first_node == 0) return 0;

	Node* pointer = first_node;

	int count = 0;

	while(pointer != 0) {

		count++;

		pointer = pointer -> next;

	}

	return count;
}

void Queue::enqueue(int id, int age)
{
	Node* temp = new Node(id, age);

	if(size() == 0) first_node = temp;

	else {

		Node* previous = 0;

		Node* pointer = first_node;

		while(pointer != 0 && (pointer -> datum.second) <= (temp -> datum.second)) {

			previous = pointer;

			pointer = pointer -> next;

		}

		if(previous == 0) {

			first_node = temp;

			temp -> next = pointer;

		}

		else {

			previous -> next = temp;

			temp -> next = pointer;

		}

	}
}

pair<int, int> Queue::dequeue()
{
	pair<int, int> datum = first_node -> datum;

	Node* temp = first_node;

	first_node = first_node -> next;

	delete temp;

	return datum;
}

int main()
{
	Queue queue;

	int m;

	cin >> m;

	while(m > 0) {

		int t = 0;

		cin >> t;

		if(t == 1) {

			int num = 0;

			cin >> num;

			while(num > 0) {

				int id = 0;

				int age = 0;

				cin >> id;

				cin >> age;

				queue.enqueue(id, age);

				num--;

			}
		}

		if(t == 2) {

			pair<int, int> temp = queue.dequeue();

			cout << temp.first << endl;

		}

		m--;
	}
}
