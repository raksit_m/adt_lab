
#include <iostream>

#include <iomanip>

using namespace std;

int main()
{
	int num;

	cin >> num;

	for(int i = 0; i < num; i++) {

		int length;

		cin >> length;

		char character[length];

		double cost[length];

		for(int j = 0; j < length; j++) {

			cin >> character[j];

			cin >> cost[j];
		}

		double sum = 0;

		int line;

		cin >> line;

		for(int k = 0; k <= line; k++) {

			string input;

			getline(cin, input);

			for(int m = 0; m < (signed)input.length(); m++) {

				for(int n = 0; n < length; n++) {

					if(character[n] == input.at(m)) {

						sum += cost[n]/100;

						break;
					}
				}
			}
		}

		cout << fixed << setprecision(2) << sum << "$" << endl;
	}
}
