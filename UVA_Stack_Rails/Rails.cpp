#include <iostream>

#include <iomanip>

using namespace std;

template <typename T>
class Stack
{
private:
	struct Node
	{
		T value;
		Node* next;
		Node(T value, Node* next=0)
		: value(value), next(next) {}
	};

	Node* top_node;

public:
	Stack();
	~Stack();

	void push(T value);
	T top();
	T pop();
	bool empty();
};

template <typename T>
Stack<T>::Stack()
{
	top_node = 0;
}

template <typename T>
Stack<T>::~Stack()
{
	while(!empty()) {
		pop();
	}
}

template <typename T>
void Stack<T>::push(T value)
{
	Node* temp = new Node(value);

	if(empty()) top_node = temp;

	else {

		temp->next = top_node;

		top_node = temp;
	}
}

template <typename T>
bool Stack<T>::empty()
{
	return top_node == 0;
}

template <typename T>
T Stack<T>::top()
{
	return top_node->value;
}

template <typename T>
T Stack<T>::pop()
{
	if(empty()) return 0;

	T temp = top();

	top_node = top_node->next;

	return temp;
}

int main()
{
	int n = 0;

	int cars[1000];

	while(true) {

		cin >> n;

		if(n == 0) break;

		while(true) {

			cin >> cars[0];

			if(cars[0] == 0) {

				cout << endl;

				break;
			}

			for(int i = 1; i < n; i++) {

				cin >> cars[i];

			}

			int currentCoach = 1, index = 0;

			Stack<int> station;

			while(currentCoach <= n) {

				station.push(currentCoach);

				while(!station.empty() && station.top() == cars[index] && index < n) {

					station.pop();

					index++;

				}

				currentCoach++;
			}

			if(station.empty()) cout << "Yes" << endl;

			else cout << "No" << endl;
		}
	}
}
