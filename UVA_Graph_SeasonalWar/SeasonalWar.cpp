#include <iostream>
#include <cstdio>
#define  DFS_WHITE -1
#define  DFS_BLACK 1
#define  NODES 26
using namespace std;
int matriz[NODES][NODES], dfs_matriz[NODES][NODES];
int dimension;

void readGraph(int dim)
{
string tmp = "", str;
int index = 0;
    for(int z=0; z<dim; z++)
    {
        cin>>str;
        tmp+=str;
    }
    for(int x=0; x<dim; x++)
    {
        for(int y=0; y<dim; y++, index++)
        {
            matriz[x][y] = tmp[index] - '0';
            if(matriz[x][y] == 1)
               dfs_matriz[x][y] = DFS_WHITE;
            else
                dfs_matriz[x][y] = 0;
        }
    }
}

void dfs(int x, int y)
{
    dfs_matriz[x][y] = DFS_BLACK;
    int a=x, b=y;
            if(a-1 >= 0)
            {
                if(dfs_matriz[a-1][b] == DFS_WHITE)
                   dfs(a-1, b);
                if(b+1 < dimension && dfs_matriz[a-1][b+1] == DFS_WHITE)
                   dfs(a-1, b+1);
                if(b-1 >= 0 && dfs_matriz[a-1][b-1] == DFS_WHITE)
                   dfs(a-1, b-1);
            }
            if(a+1 < dimension)
            {
                if(b+1 < dimension && dfs_matriz[a+1][b+1] == DFS_WHITE)
                   dfs(a+1, b+1);
                if(b-1 >= 0 && dfs_matriz[a+1][b-1] == DFS_WHITE)
                   dfs(a+1, b-1);
                if(dfs_matriz[a+1][b] == DFS_WHITE)
                   dfs(a+1, b);
            }
            if(b-1 >= 0 && dfs_matriz[a][b-1] == DFS_WHITE)
               dfs(a,b-1);
            if(b+1 < dimension && dfs_matriz[a][b+1] == DFS_WHITE)
               dfs(a,b+1);
}

int main()
{
int cc, casos = 1;
while(scanf("%d", &dimension) != EOF)
{
    readGraph(dimension);
    cc = 0;
    for(int x=0; x<dimension; x++)
    {
        for(int y=0; y<dimension; y++)
        {
            if(matriz[x][y] == 1 && dfs_matriz[x][y] == DFS_WHITE)
            {
                dfs(x,y);
                cc++;
            }
        }
    }
    printf("Image number %d contains %d war eagles.\n", casos++, cc);
}
return 0;
}
