
#include <iostream>

using namespace std;

int main()
{
	int cases;

	cin >> cases;

	int* answer = new int[cases];

	for(int i = 0; i < cases; i++) {

		int days, parties, hartalDays = 0;

		cin >> days;

		cin >> parties;

		int* hartal = new int[parties];

		for(int j = 0; j < parties; j++) cin >> hartal[j];

		for(int k = 0, currentDay = 1; currentDay <= days; k++, k %= 7, currentDay++)

			if((k != 5) && (k != 6)) {

				for(int m = 0; m < parties; m++) {

					if(!(currentDay % hartal[m])) {

						hartalDays++;

						break;

					}
				}
			}

		answer[i] = hartalDays;
	}

	for(int i = 0; i < cases; i++) cout << answer[i] << endl;
}
