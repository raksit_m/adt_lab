
#include <iostream>

#include <vector>

using namespace std;

int n;

bool check(int m)
{
	vector<int> region;

	for(int i = 1; i <= n; i++) {

		region.push_back(i);

	}

	int num = 0;

	while(region[num] != 13) {

		region.erase(region.begin() + num);

		num += m-1;

		if(num >= (int)region.size()) num %= (int)region.size();

	}

	return region.size() == 1;
}

int main()
{
	cin >> n;

	while(n != 0) {

		int m = 1;

		while(!check(m)) m++;

		cout << m << endl;

		cin >> n;
	}
}
