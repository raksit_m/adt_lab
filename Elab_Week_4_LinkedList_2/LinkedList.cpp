
#include <iostream>

using namespace std;

typedef int valueType;

struct ListNode
{
	valueType val;

	ListNode* next;

	ListNode(valueType val, ListNode* next=0)
	: val(val), next(next) {}
};

class LinkedList
{
private :
	ListNode* header;
	ListNode* tail;
	void free_list();

public :
	LinkedList();
	LinkedList(const LinkedList& other);
	~LinkedList();
	void print_list();
	void append(valueType x); // insert from back
	void insert_front(valueType x); // insert from front
	void delete_front(); // delete from front
	valueType get_front();
	bool is_empty();

	LinkedList& operator=(const LinkedList& other);
};

LinkedList::LinkedList()
{
	header = new ListNode(0);
	tail = header; // new list: tail is on the same position as header
}

LinkedList::LinkedList(const LinkedList& other)
{
	header = new ListNode(0);
	tail = header;
	ListNode* p = other.header -> next;
	while(p) {
		append(p->val);
		p = p->next;
	}
}

LinkedList::~LinkedList()
{
	free_list();
}

void LinkedList::print_list()
{
	ListNode* node = header->next;
	while(node != 0) {
		cout << node->val << endl;
		node = node->next;
	}
}

void LinkedList::append(valueType x)
{
	ListNode* temp = new ListNode(x);
	tail->next = temp; // next element of old tail is new tail.
	tail = temp; // after adding new tail, it will become a current tail.

}

bool LinkedList::is_empty()
{
	return header->next == 0;

}

LinkedList& LinkedList:: operator=(const LinkedList& other)
{

	if(this == &other) {
		return *this;
	}

	free_list();

	header = new ListNode(0);
	tail = header;
	ListNode* p = other.header -> next;
	while(p) {
		append(p->val);
		p = p->next;
	}
	return *this;
}

void LinkedList::insert_front(valueType x)
{
	// We need to check whether list is empty or not because:
	// 1.) If list is not empty, when we insert new element from front, the tail would be the same.
	// 2.) If list is empty, it means that the new element will be a new tail.

	if(is_empty()) {
		ListNode* temp = new ListNode(x);
		temp->next = header->next;
		header->next = temp;
		tail = temp;
	}

	else {
		ListNode* temp = new ListNode(x);
		temp->next = header->next;
		header->next = temp;
	}

}

void LinkedList::delete_front()
{
	if(is_empty()) {
		throw "Error: list is empty.";
	}
	ListNode* front = header->next;

	header->next = front->next; // before deleting front, we need to bound header and the next element of front.

	if(is_empty()) {
		tail = header;
	}

	delete front;

}

valueType LinkedList::get_front()
{
	return header->next->val;

}

void LinkedList::free_list()
{
	while(header != 0) {
		ListNode* temp = header->next;
		delete header;
		header = temp;
	}

}

int main()
{
	LinkedList l1;

	l1.append(10);
	cout << l1.get_front() << endl;
	l1.insert_front(20);
	cout << l1.get_front() << endl;
	l1.delete_front();
	cout << l1.get_front() << endl;
	cout << (l1.is_empty() ? "yes" : "no") << endl;
	l1.delete_front();
	cout << (l1.is_empty() ? "yes" : "no") << endl;
	l1.append(100);
	cout << l1.get_front() << endl;
	l1.insert_front(200);
	cout << l1.get_front() << endl;
	l1.append(1000);
	l1.print_list();
	l1.delete_front();
	l1.delete_front();
	l1.print_list();
	l1.delete_front();
	cout << (l1.is_empty() ? "yes" : "no") << endl;
}
