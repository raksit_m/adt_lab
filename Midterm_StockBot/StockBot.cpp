
#include <iostream>

using namespace std;

int main()
{
	int n, k = 0;

	cin >> n >> k;

	int stock[n];

	for(int i = 0; i < n; i++) {

		stock[i] = 0;

	}

	for(int i = 0; i < n; i++) {

		cin >> stock[i];

	}

	int sum = 0;

	int temp = 0;

	int remaining = 0;

	for(int i = 1; i < n; i++) {

		if(stock[i] - stock[i - 1] >= k && remaining == 0) {

			temp = stock[i];

			remaining ++;

		}

		else if(stock[i - 1] - stock[i] >= k && remaining > 0) {

			sum += stock[i] - temp;

			remaining --;

		}
	}

	cout << sum << endl;
}
