
#include <iostream>

#include <math.h>

using namespace std;

int partition(int array[], int left, int right)
{
	int pivot = array[left];

	int i = left;

	for(int j = left + 1; j <= right; j++) {

		if(array[j] <= pivot) {

			i++;

			int temp = array[i];

			array[i] = array[j];

			array[j] = temp;
		}
	}

	int temp = array[left];

	array[left] = array[i];

	array[i] = temp;

	return i;

}

void quickSort(int array[], int left, int right)
{
	if(left < right) {

		int index = partition(array, left, right);

		quickSort(array, left, index - 1);

		quickSort(array, index + 1, right);

	}
}

int main()
{
	int n;

	cin >> n;

	int array[n];

	for(int i = 0; i < n; i++) {

		cin >> array[i];
	}

	quickSort(array, 0, n-1);

	int minDiff = 0;

	for(int i = 0; i < n-1; i++) {

		if(abs(array[i+1] - array[i]) < minDiff || i == 0) {

			minDiff = abs(array[i+1] - array[i]);

		}
	}

	cout << minDiff << endl;
}
