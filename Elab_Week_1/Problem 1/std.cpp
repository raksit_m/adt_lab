
#include <iostream>

#include <cstdlib>

using namespace std;

int gcd(int num1, int num2) {

	if (num2 == 0) return num1;

	return gcd(num2, num1 % num2);
}


int main() {

	int num1;

	cin >> num1;

	int num2;

	cin >> num2;

	int ans1;

	ans1 = abs(num1);

	ans1 /= gcd(num1, num2);

	int ans2;

	ans2 = abs(num2);

	ans2 /= gcd(num1, num2);

	cout << ans1 << "/" << ans2 << endl;

}

