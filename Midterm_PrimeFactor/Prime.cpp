
#include <iostream>

using namespace std;

bool isPrime(int n) {

	for(int i = 2; i < n; i++) {

		if(n % i == 0 && n != 2) return false;

	}

	return true;
}

int main()
{
	int n;

	cin >> n;

	int count = 0;

	for(int i = 2; i < n; i++) {

		if(n % i == 0 && isPrime(i)) count++;

	}

	cout << count << endl;
}
