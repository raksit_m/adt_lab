#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include "disjoint_set.h"

#define MAX 100

void print_maze(int num_rows, int num_cols, int *v_wall, int *h_wall) {
	int i, j;

	printf("*");
	for (j=0; j<num_cols; j++)
		printf("**");
	printf("\n");

	for (i=0; i<num_rows; i++) {
		printf("*");
		for (j=0; j<num_cols; j++)
			if (v_wall[i*num_cols + j] == 0)
				printf(" *");
			else
				printf("  ");
		printf("\n");
		printf("*");
		for (j=0; j<num_cols; j++)
			if (h_wall[i*num_cols + j] == 0)
				printf("**");
			else
				printf(" *");
		printf("\n");
	}
}

void print_2Darray(int *A, int r, int c) {
	int i, j;
	for (i=0; i<r; i++) {
		for (j=0; j<c; j++)
			printf("%d ", A[i*c + j]);
		printf("\n");
	}
}

// swaps array elements i and j
void exch(int *a, int i, int j) {

	int temp = a[i];

	a[i] = a[j];

	a[j]= temp;
}

// take as input an array of int and rearrange them in random order
void shuffle(int *a, int N) {

	if (N > 1) {

		for (int i = 0; i < N; i++) {

			int j = rand() % N ;
			while(j == i) j = rand()% N;

			exch(a, i, j);

		}
	}
}

// take as input an array of int and print them out to standard output
void show(int *a, int N) {
	int i;
	for (i = 0; i < N; i++) {
		printf("%d ", a[i]);
	}
	printf("\n");
}

void generate_maze(int *maze_array, int maze_size, int *v_wall_num, int *v_wall, int *h_wall_num, int *h_wall, int num_rows, int num_cols) {

	for(int i = 0; i < maze_size; i++) {

		if(h_wall_num[i] < (num_rows-1)*num_cols) {

			int cell_1 = find(maze_array, h_wall_num[i]);

			int cell_2 = find(maze_array, h_wall_num[i]+num_cols);

			if(cell_1 != cell_2) {

				q_union(maze_array, cell_1, cell_2);

				h_wall[h_wall_num[i]]++;

			}
		}

		if((v_wall_num[i] + 1) % num_cols != 0) {

			int cell_1 = find(maze_array, v_wall_num[i]);

			int cell_2 = find(maze_array, v_wall_num[i]+1);

			if(cell_1 != cell_2) {

				q_union(maze_array, cell_1, cell_2);

				v_wall[v_wall_num[i]]++;
			}
		}
	}

	for(int i = 0; i < maze_size; i++) {

		printf("%d ", v_wall[i]);
	}

	printf("\n");

	for(int i = 0; i < maze_size; i++) {

		printf("%d ", h_wall[i]);
	}

	printf("\n");
}

int main(int argc, char **argv) {
	int i, j;
	int num_rows, num_cols;
	int v_wall[MAX*MAX], h_wall[MAX*MAX];
	int v_wall_num[MAX*MAX], h_wall_num[MAX*MAX];
	int maze_array[MAX*MAX];
	int maze_size;
//
	srand(time(NULL));
	num_rows = atoi(argv[1]);
	num_cols = atoi(argv[2]);
	maze_size = num_rows*num_cols;
	for (i=0; i<num_rows; i++)
		for (j=0; j<num_cols; j++) {
			v_wall[i*num_cols + j] = 0;
			h_wall[i*num_cols + j] = 0;
		}
	print_maze(num_rows, num_cols, v_wall, h_wall);
	for (i=0; i<num_rows; i++)
		for (j=0; j<num_cols; j++) {
			v_wall_num[i*num_cols + j] = i*num_cols + j;
			h_wall_num[i*num_cols + j] = i*num_cols + j;
		}
	shuffle(v_wall_num, maze_size);
	shuffle(h_wall_num, maze_size);
//	for(int i =0;i<16;i++){
//		printf("%d ",v_wall_num[i]);
//	}
//	printf("\n");
//	for(int i =0;i<16;i++){
//			printf("%d ",h_wall_num[i]);
//		}
//	printf("\n");
	initializing_set(maze_array, maze_size);
	generate_maze(maze_array, maze_size, v_wall_num, v_wall, h_wall_num, h_wall, num_rows, num_cols);
	printf("\n");
	printf("Generated maze:\n");
	print_maze(num_rows, num_cols, v_wall, h_wall);
	return 0;
}


