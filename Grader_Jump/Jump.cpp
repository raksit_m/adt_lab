
#include <iostream>

#include <vector>

#include <list>

#include <math.h>

#define MAX_N  1000

using namespace std;

vector<int> adj[MAX_N];

pair<int, int> node[MAX_N];

int deg[MAX_N];

bool visited[MAX_N] = {false};

bool isReachable(int v, int w, int r) {

//	cout << node[w].first << " " << node[w].second << endl;
//
//	cout << node[v].first << " " << node[v].second << endl;

//	cout << abs(((node[w].first - node[v].first)*(node[w].first - node[v].first)) + ((node[w].second - node[v].second)*(node[w].second - node[v].second))) << endl;

	if(abs(((node[w].first - node[v].first)*(node[w].first - node[v].first)) + ((node[w].second - node[v].second)*(node[w].second - node[v].second))) <= (r*r)) {

		return true;
	}

	return false;
}

void addEdge(int v, int w) {

	adj[v].push_back(w);

	adj[w].push_back(v);

	deg[v]++;

	deg[w]++;

}

//void bfs(int s) {
//
//	list<int> queue;
//
//	visited[s] = true;
//
//	queue.push_back(s);
//
//	list<int>::iterator i;
//
//	while(!queue.empty()) {
//
//		s = queue.front();
//
//		queue.pop_front();
//
//		for(i = adj[s].begin(); i != adj[s].end(); i++)
//		{
//			if(!visited[*i])
//			{
//				visited[*i] = true;
//
//				queue.push_back(*i);
//			}
//		}
//	}
//}

int dfs(int n, int u)
{
	int count = 0;

	visited[u] = true;

	for(int i = 0; i < deg[u]; i++) {

		int v = adj[u][i];

		if(visited[n+1]) {

			break;
		}

		else if(!visited[v]) {

			count++;

			return dfs(n, v);
		}
	}

	return count;
}

int main()
{
	int n, r;

	cin >> n >> r;

	node[0] = make_pair(0, 0);

	node[n+1] = make_pair(100, 100);

	for(int i = 1; i <= n; i++) {

		int x, y = 0;

		scanf("%d %d", &x, &y);

		node[i] = make_pair(x, y);

	}

	for(int i = 0; i <= n+1; i++) {

		printf("(%d, %d)\n", node[i].first, node[i].second);
	}

	for(int i = 0; i <= n+1; i++) {

		for(int j = 0; j <= n+1; j++) {

			if(isReachable(i, j, r) && i != j) {

				addEdge(i, j);
			}
		}
	}

	int count = dfs(n, 0);

	cout << count;
}
