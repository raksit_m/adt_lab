
#include <iostream>

using namespace std;

int main()
{
	int stack[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	int n = 0;

	cin >> n;

	while(n > 0) {

		char type = 0;

		int position = 0;

		cin >> type >> position;

		if(type == '-') {

			stack[position - 1]++;

			stack[position + 0]++;

			stack[position + 1]++;

			stack[position + 2]++;

			int maxHeight = 0;

			for(int i = -1; i < 3; i++) {

				if(i == -1 || stack[position + i] > maxHeight) maxHeight = stack[position + i];

			}

			for(int i = -1; i < 3; i++) {

				stack[position + i] = maxHeight;

			}

		}

		else if(type == 'i') {

			stack[position - 1] += 4;

		}

		else if(type == 'o') {

			stack[position - 1] += 2;

			stack[position - 0] += 2;

			if(stack[position - 1] > stack[position - 0]) stack[position - 0] = stack[position - 1];

			else stack[position - 1] = stack[position - 0];

		}

		n--;
	}

	int max = 0;

	for(int i = 0; i < 10; i++) {

		if(i == 0) max = stack[i];

		else {

			if(stack[i] > max) max = stack[i];

		}
	}

	cout << max << endl;

}
