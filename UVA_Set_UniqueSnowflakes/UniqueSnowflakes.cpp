
#include <iostream>

#include <map>

#include <math.h>

using namespace std;

int main()
{
	int n = 0;

	cin >> n;

	map<int, int> snowflakes;

	while(n > 0) {

		int k;

		cin >> k;

		snowflakes.clear();

		int ans = 0, numSeenSoFar = 0, lastUnique = 0;

		for(int i = 1; i <= k; i++) {

			int x;

			cin >> x;

			int value = snowflakes[x];

			if(value != 0) {

				lastUnique = max(lastUnique, value);

				numSeenSoFar = i-lastUnique-1;
			}

			numSeenSoFar++;

			snowflakes[x] = i;

			ans = max(ans, numSeenSoFar);

		}

		cout << ans << endl;

		n--;
	}
}
