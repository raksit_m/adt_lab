
#include <iostream>

#include <cmath>

#include <set>

#include <algorithm>

using namespace std;

int main()
{
	int n, m;

	cin >> n >> m;

	set<int> x;

	for(int i = 0; i < n; i++) {

		int temp;

		cin >> temp;

		x.insert(temp);

	}

	for(int i = 0; i < m; i++) {

		int y;

		cin >> y;

		std::set<int>::iterator low = x.lower_bound(y);

		std::set<int>::iterator up = low;

		if(*low == y) low++;

		low--;

//		cout << *low << " " << *up << endl;

		if(up == x.end()) {

			cout << *low++ << endl;
		}

		else {

			if((abs(*low - y) <= abs(*up - y))) {

				cout << *low << endl;
			}

			else cout << *up << endl;

		}
	}
}
