
#include <list>

#include <iostream>

using namespace std;

void sorted_insert(list<int>& lst, int x)
{
	list<int>::iterator li = lst.begin();

	while(li != lst.end() && (*li < x)) {
		++li;
	}
	lst.insert(li, x);
}

int main()
{
	list<int> mylist;

//	mylist.push_back(10);
//
//	mylist.push_back(20);
//
//	mylist.push_front(100);
//
//	mylist.push_back(15);
//
//	list<int>::iterator ii = mylist.begin();
//	++ii; ++ii;
//	mylist.insert(ii, 10000);

	sorted_insert(mylist, 10);
	sorted_insert(mylist, 1000);
	sorted_insert(mylist, 100);
	sorted_insert(mylist, 5);
	sorted_insert(mylist, 200);
	sorted_insert(mylist, 250);
	sorted_insert(mylist, 10);
	sorted_insert(mylist, 5);
	sorted_insert(mylist, 1000);

	list<int>::iterator li = mylist.begin();

	while(li != mylist.end()) {
		cout << *li << endl;
		++li;
	}
}
