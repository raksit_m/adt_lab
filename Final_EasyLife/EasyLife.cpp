
#include <iostream>

using namespace std;

int main()
{
	int n;

	cin >> n;

	int day[31] = {0};

	int count = 0;

	for(int i = 0; i < n; i++) {

		int a, b, h;

		cin >> a >> b >> h;

		int temp[31] = {0};

		for(int k = 0; k < 31; k++) {

			temp[k] = day[k];
		}

		for(int j = a-1; j < b; j++) {

			day[j] += h;

			if(day[j] > 6) {

				for(int k = 0; k < 31; k++) {

					day[k] = temp[k];
				}

				count--;

				break;
			}
		}

		count++;

//		for(int k = 0; k < 31; k++) {
//
//				cout << day[k] << " ";
//			}
//
//			cout << endl;
	}

	cout << count << endl;
}
