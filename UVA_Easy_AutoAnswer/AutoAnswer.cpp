
#include <iostream>

#include <math.h>

using namespace std;

int calculate(int n)
{
	return (((n*567/9) + 7492)*235/47)-498;
}

int find_tens_column(int result)
{
	return abs((result / 10) % 10);
}

int main()
{
	int length;

	cin >> length;

	int array [length];

	for(int i = 0; i < length; i++) {

		cin >> array[i];
	}

	for(int i = 0; i < length; i++) {

		cout << find_tens_column(calculate(array[i])) << endl;

	}

}
