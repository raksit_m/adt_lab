
#include <iostream>

#include <math.h>

#include <set>

using namespace std;

int main()
{
	int w, l, a;

	cin >> w >> l >> a;

	int area[w][l];

	for(int i = 0; i < w; i++) {

		for(int j = 0; j < l; j++) {

			cin >> area[i][j];
		}
	}

	set<int> sum;

	int temp = 0;

	for (int i = 0; i < 3; i++) {

		for (int j = 0; j < 3; j++) {

			temp += area[i][j];

		}
	}

	cout << temp << endl;

}
