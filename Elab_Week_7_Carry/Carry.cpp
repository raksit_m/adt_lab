
#include <iostream>

#include <math.h>

using namespace std;

int main()
{
	while(!cin.eof()) {

		int a, b;

		int A[32], B[32], ans[32], sum, answer = 0;

		cin >> a;

		if(cin.eof()) break;

		cin >> b;

		for(int i = 31 ; i >= 0 ; i--) {

			if(a >= pow(2, i)) {

				A[i] = 1;

				a -= pow(2, i);

			}

			else A[i] = 0;

			if(b>=pow(2, i)) {

				B[i] = 1;

				b -= pow(2, i);

			}

			else B[i] = 0;

			sum = A[i] + B[i];

			ans[i] = sum % 2;

			answer += ans[i] * pow(2, i);

		}

		cout << answer << endl;
	}

	return 0;
}
