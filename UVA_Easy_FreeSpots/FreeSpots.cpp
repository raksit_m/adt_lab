#include <iostream>

using namespace std;

int main()
{
	int x1, y1, x2, y2;

	int w, h, n;

	cin >> w >> h >> n;

	while(w != 0 && h != 0) {

		bool board [1000][1000] = {false};

		for(int i = 0; i < n; i++) {

			cin >> x1 >> y1 >> x2 >> y2;

			for(int j = min(x1, x2); j <= max(x1, x2); j++) {

				for(int k = min(y1, y2); k <= max(y1, y2); k++) board[j][k] = true;

			}

		}

		int count = 0;

		for(int i = 1; i <= w; i++) {

			for(int j = 1; j <= h; j++) {

				if(board[i][j] == false) count++;

			}
		}

		if ( count == 0 ) cout << "There is no empty spots." << endl;

		else if ( count == 1 ) cout << "There is one empty spot." << endl;

		else cout << "There are " << count << " empty spots." << endl;

		cin >> w >> h >> n;

	}
}
