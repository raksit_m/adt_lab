
#include <iostream>

#include <set>

using namespace std;

int main()
{
	int n, x, k;

	long sum = 0;

	multiset<int> bills;

	cin >> n;

	while(n != 0) {

		sum = 0;

		bills.clear();

		for(int i = 0; i < n; i++) {

			cin >> k;

			for(int j = 0; j < k; j++) {

				cin >> x;

				bills.insert(x);
			}

			std::set<int>::iterator high = bills.end();

			std::set<int>::iterator low = bills.begin();

			high--;

			sum += *high - *low;

			bills.erase(high);

			bills.erase(low);
		}

		cout << sum << endl;

		cin >> n;
	}
}
