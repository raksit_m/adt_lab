
#include <iostream>

#include <list>

#include <vector>

using namespace std;

#define MAX_N  1000

list<int> adj[MAX_N];

int height[MAX_N][MAX_N];

int deg[MAX_N];

void addEdge(int v, int w)
{
    adj[v].push_back(w); // Add w to v�s list.

    adj[w].push_back(v);
}

bool isReachable(int x, int y, int h)
{
	bool isReachable = false;

	if (x == y) return true;

	bool visited[MAX_N];

	for (int i = 0; i < MAX_N; i++) {

		visited[i] = false;
	}

	list<int> queue;

	visited[x] = true;

	queue.push_back(x);

	list<int>::iterator i;

	while (!queue.empty())
	{
		x = queue.front();

		queue.pop_front();

		for (i = adj[x].begin(); i != adj[x].end(); ++i) {

			if (*i == y && height[x][*i] >= h) {

				isReachable = true;

				break;
			}

			// Else, continue to do BFS
			if (!visited[*i])
			{
				visited[*i] = true;

				queue.push_back(*i);
			}
		}
	}

	return isReachable;
}

int main()
{
	int n, m, k;

	cin >> n >> m >> k;

	for(int i = 0; i < n; i++) {

		int c, d, h;

		scanf("%d %d %d",&c, &d, &h); c--; d--;

		height[c][d] = h;

		height[d][c] = h;

		addEdge(c, d);

	}

	for(int i = 0; i < k; i++) {

		int x, y, z;

		cin >> x >> y >> z;

		if(isReachable(x-1, y-1, z)) {

			cout << "yes" << endl;
		}

		else cout << "no" << endl;
	}
}
