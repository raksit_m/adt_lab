
#include <iostream>

using namespace std;

int decimal_to_binary_count(int decimal) {

	int count = 0;

	while(decimal != 0) {

		if(decimal % 2 == 1) {

			count++;
		}

		decimal /= 2;
	}

	return count;
}

int hexadecimal_to_binary_count(int hexadecimal) {

	int count = 0;

	while(hexadecimal != 0) {

		int temp = decimal_to_binary_count(hexadecimal % 10);

		count += temp;

		hexadecimal /= 10;
	}

	return count;
}

int main() {

	int n;

	cin >> n;

	int decimal[n];

	for(int i = 0; i < n; i++) {

		cin >> decimal[i];

	}

	for(int i = 0; i < n; i++) {

		int b1 = decimal_to_binary_count(decimal[i]);

		int b2 = hexadecimal_to_binary_count(decimal[i]);

		cout << b1 << " " << b2 << endl;

	}
}

