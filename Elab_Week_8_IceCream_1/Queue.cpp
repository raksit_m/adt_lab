
#include <iostream>

using namespace std;

class Queue
{
private:

	struct Node {

		int id;

		Node* next;

		Node(int id, Node* next = 0)

		:	id(id), next(next) {}

	};

	Node* first_node;

public:

	Queue();

	~Queue();

	void enqueue(int id);

	int dequeue();

	int size();
};

Queue::Queue()
{
	first_node = 0;
}

Queue::~Queue()
{
	while(first_node != 0) dequeue();
}

int Queue::size()
{
	if(first_node == 0) return 0;

	Node* pointer = first_node;

	int count = 0;

	while(pointer != 0) {

		count++;

		pointer = pointer -> next;

	}

	return count;
}

void Queue::enqueue(int id)
{
	Node* temp = new Node(id);

	if(size() == 0) first_node = temp;

	else {

		Node* pointer = first_node;

		while(pointer -> next != 0) pointer = pointer -> next;

		pointer -> next = temp;

	}
}

int Queue::dequeue()
{
	int datum = first_node -> id;

	Node* temp = first_node;

	first_node = first_node -> next;

	delete temp;

	return datum;
}

int main()
{
	Queue queue;

	int m;

	cin >> m;

	while(m > 0) {

		int t = 0;

		cin >> t;

		if(t == 1) {

			int num = 0;

			cin >> num;

			while(num > 0) {

				int id = 0;

				cin >> id;

				queue.enqueue(id);

				num--;

			}
		}

		if(t == 2) {

			cout << queue.dequeue() << endl;

		}

		m--;
	}

	cout << queue.size() << endl;
}

