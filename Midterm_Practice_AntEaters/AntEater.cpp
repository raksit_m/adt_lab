
#include <iostream>

using namespace std;

int main()
{
	int n = 0;

	int sum = 0, temp = 0;

	cin >> n;

	while(n > 0) {

		int ant = 0;

		cin >> ant;

		if(temp < 1000 && ant <= 1000) {

			sum += ant;

			temp = ant;

		}

		else if(temp < 1000 && ant > 1000) {

			sum += 1000;

			temp = 1000;
		}

		else temp = 0;

		n--;
	}

	cout << sum << endl;
}
