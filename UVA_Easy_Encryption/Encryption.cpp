
#include <iostream>

#include <sstream>

#include <math.h>

#include <string>

using namespace std;

std::string decimal_to_binary(int decimal)
{
	std::string binary;

	while(decimal != 0) {

		std::stringstream s;

		s << decimal % 2;

		binary += s.str();

		decimal /= 2;
	}

	return binary;
}

std::string hexadecimal_to_binary(int decimal)
{
	std::string binary;

	int digits = 0;

	int temp = decimal;

	while(temp != 0) {

		temp /= 10;

		digits++;

	}

	digits--;

	while(decimal != 0) {

		std::stringstream s;

		s << decimal_to_binary(decimal / (int)pow(10, digits));

		binary += s.str();

		decimal %= decimal / (int)pow(10, digits) * (int)pow(10, digits);

		digits--;
	}

	return binary;
}

int count_one(std::string binary)
{
	int count = 0;

	for(int i = 0; i < (signed)binary.length(); i++) {

		char temp = binary.at(i);

		if(temp == '1') {
			count++;
		}
	}

	return count;

}

int main()
{
	int length;

	cin >> length;

	int array [length];

	for(int i = 0; i < length; i++) {

		cin >> array[i];
	}

	for(int i = 0; i < length; i++) {

		string binary_1 = decimal_to_binary(array[i]);

		string binary_2 = hexadecimal_to_binary(array[i]);

		int count_1 = count_one(binary_1);

		int count_2 = count_one(binary_2);

		cout << count_1 << " " << count_2 << endl;
	}
}
