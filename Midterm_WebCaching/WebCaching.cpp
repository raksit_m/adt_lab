
#include <iostream>

using namespace std;

class Queue
{
private:

	struct Node {

		int id;

		Node* next;

		Node(int id, Node* next = 0)

		:	id(id), next(next) {}

	};

	Node* first_node;

public:

	Queue();

	~Queue();

	void enqueue(int id);

	int dequeue();

	int get(int index);

	int size();
};

Queue::Queue()
{
	first_node = 0;
}

Queue::~Queue()
{
	while(first_node != 0) dequeue();
}

int Queue::get(int index) {

	Node* temp = first_node;

	while(temp->next != 0 && index > 0) {

		temp = temp->next;

		index--;
	}

	return temp->id;
}

int Queue::size()
{
	if(first_node == 0) return 0;

	Node* pointer = first_node;

	int count = 0;

	while(pointer != 0) {

		count++;

		pointer = pointer -> next;

	}

	return count;
}

void Queue::enqueue(int id)
{
	Node* temp = new Node(id);

	if(size() == 0) first_node = temp;

	else {

		Node* pointer = first_node;

		while(pointer -> next != 0) pointer = pointer -> next;

		pointer -> next = temp;

	}
}

int Queue::dequeue()
{
	int datum = first_node -> id;

	Node* temp = first_node;

	first_node = first_node -> next;

	delete temp;

	return datum;
}

int main()
{
	int n, m = 0;

	int miss = 0;

	cin >> n >> m;

	Queue pages;

	while(m > 0) {

		int cache = 0;

		bool isHit = false;

		cin >> cache;

		for(int i = 0; i < pages.size(); i++) {

			if(cache == pages.get(i)) {

				miss --;

				isHit = true;

				break;
			}
		}

		miss ++;

		if(!isHit) {

			pages.enqueue(cache);
		}

		if(pages.size() > n) {

			pages.dequeue();

		}

		m--;
	}

	cout << miss << endl;
}
