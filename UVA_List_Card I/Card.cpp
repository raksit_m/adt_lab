
#include <iostream>

using namespace std;


#include <iostream>

using namespace std;

class Queue
{
private:

	struct Node {

		int id;

		Node* next;

		Node(int id, Node* next = 0)

		:	id(id), next(next) {}

	};

	Node* first_node;

public:

	Queue();

	~Queue();

	void enqueue(int id);

	int front();

	int dequeue();

	int size();
};

Queue::Queue()
{
	first_node = 0;
}

Queue::~Queue()
{
	while(first_node != 0) dequeue();
}

int Queue::size()
{
	if(first_node == 0) return 0;

	Node* pointer = first_node;

	int count = 0;

	while(pointer != 0) {

		count++;

		pointer = pointer -> next;

	}

	return count;
}

int Queue::front()
{
	return first_node->id;
}

void Queue::enqueue(int id)
{
	Node* temp = new Node(id);

	if(size() == 0) first_node = temp;

	else {

		Node* pointer = first_node;

		while(pointer -> next != 0) pointer = pointer -> next;

		pointer -> next = temp;

	}
}

int Queue::dequeue()
{
	int datum = first_node -> id;

	Node* temp = first_node;

	first_node = first_node -> next;

	delete temp;

	return datum;
}

int main()
{
	Queue queue;

	int n, card = 0;

	cin >> n;

	while(n != 0) {

		for(int i = 1; i <= n; i++) {

			queue.enqueue(i);

		}

		cout << "Discarded cards:";

		while(queue.size() > 1) {

			cout << " " << queue.dequeue();

			card = queue.dequeue();

			if(queue.size() != 0) cout << ",";

			queue.enqueue(card);

		}

		cout << endl;

		cout << "Remaining card: " << queue.dequeue() << endl;

		cin >> n;

	}
}
