
#include <iostream>

#include <map>

#include <vector>

#include <math.h>

#include <algorithm>

using namespace std;

int main()
{
	int n;

	map<vector<int>, int> map;

	cin >> n;

	while(n != 0) {

		map.clear();

		for(int i = 0; i < n; i++) {

			vector<int> v(5);

			for(int j = 0; j < 5; j++) {

				cin >> v[j];
			}

			sort(v.begin(), v.end());

			map[v]++;
		}

		int maximum = 0;

		for(std::map<vector<int>,int>::iterator it = map.begin(); it != map.end(); it++) {

			maximum = max(maximum, it->second);

		}

		int count = 0;

		for(std::map<vector<int>,int>::iterator it = map.begin(); it != map.end(); it++) {

			if(it->second == maximum) count++;

		}

		cout << maximum*count << endl;

		cin >> n;
	}
}
