
#include <iostream>

using namespace std;

int main() {

	int i,j;

	while(!cin.eof()) {

		cin >> i;

		if(cin.eof()) {
			break;
		}

		cin >> j;

		int length = 0, max_length = 0;

		int max, min;

		if(i > j) {

			max = i;

			min = j;
		}

		else if (i < j) {

			max = j;

			min = i;
		}

		else if (i == j) {

			max = i;

			min = j;

		}

		for(int num = min; num <= max; num++) {

			int temp = num;

			while(temp != 1) {

				if(temp % 2 != 0) {

					temp = (3*temp) + 1;

				}

				else temp /= 2;

				length++;
			}

			if(length >= max_length) max_length = length + 1;

			length = 0;
		}

		cout << i << " " << j << " " << max_length << endl;

		max_length = 0;

	}

	return 0;
}

