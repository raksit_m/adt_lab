
#include <iostream>

using namespace std;

class Queue
{
private:

	struct Node {

		pair<int, int> datum;

		Node* next;

		Node(int id, int flavor, Node* nxt=0) {

			datum = make_pair(id, flavor);

			next = nxt;

		}

	};

	Node* first_node;

public:

	Queue();

	~Queue();

	void enqueue(int id, int flavor);

	pair<int, int> dequeue();

	int size();
};

Queue::Queue()
{
	first_node = 0;
}

Queue::~Queue()
{
	while(first_node != 0) dequeue();
}

int Queue::size()
{
	if(first_node == 0) return 0;

	Node* pointer = first_node;

	int count = 0;

	while(pointer != 0) {

		count++;

		pointer = pointer -> next;

	}

	return count;
}

void Queue::enqueue(int id, int flavor)
{
	Node* temp = new Node(id, flavor);

	if(size() == 0) first_node = temp;

	else {

		Node* pointer = first_node;

		while(pointer -> next != 0) pointer = pointer -> next;

		pointer -> next = temp;

	}
}

pair<int, int> Queue::dequeue()
{
	pair<int, int> datum = first_node -> datum;

	Node* temp = first_node;

	first_node = first_node -> next;

	delete temp;

	return datum;
}

int main()
{
	Queue queue;

	int counter [20] = {0};

	int m;

	cin >> m;

	while(m > 0) {

		int t = 0;

		cin >> t;

		if(t == 1) {

			int num = 0;

			cin >> num;

			while(num > 0) {

				int id = 0;

				int flavor = 0;

				cin >> id;

				cin >> flavor;

				queue.enqueue(id, flavor);

				num--;

			}
		}

		if(t == 2) {

			pair<int, int> temp = queue.dequeue();

			counter[temp.second - 1] ++;

			cout << temp.first << endl;

		}

		m--;
	}

	cout << queue.size() << endl;

	for(int i = 0; i < 20; i++) {

		cout << counter[i] << " ";

	}
}
