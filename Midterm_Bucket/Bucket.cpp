
#include <iostream>

using namespace std;

int main()
{
	int n;

	cin >> n;

	int jar[n];

	int bucket[5] = {0};

	int tank = 0;

	int minBucket = 0;

	int index = 0;

	for(int i = 0; i < n; i++) {

		for(int j = 0; j < 5; j++) {

			if(j == 0 || bucket[j] < minBucket) {

				minBucket = bucket[j];

				index = j;
			}

		}

		cin >> jar[i];

		bucket[index] += jar[i];

		if(bucket[index] < 1000) {

			index++;
		}

		else {

			bucket[index] = 0;

			tank += 1000;

		}

		//		for(int j = 0; j < 5; j++) {
		//
		//			cout << bucket[j] << " ";
		//		}
		//
		//		cout << endl;

	}

	cout << tank << endl;


}
