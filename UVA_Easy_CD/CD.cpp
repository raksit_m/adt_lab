
#include <iostream>

#include <set>

using namespace std;

int main()
{
	int n, m;

	cin >> n >> m;

	while(n != 0 && m != 0) {

		set<int> cd;

		for(int i = 0; i < n; i++) {

			int jack;

			cin >> jack;

			cd.insert(jack);
		}

		int count = 0;

		for(int j = 0; j < m; j++) {

			int tempSize = cd.size();

			int jill;

			cin >> jill;

			cd.insert(jill);

			if(tempSize == cd.size()) count++;
		}

		cout << count << endl;

		cin >> n >> m;
	}
}
